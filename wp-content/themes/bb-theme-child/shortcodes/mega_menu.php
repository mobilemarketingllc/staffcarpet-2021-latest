<div class="sc_mega_menu">
    <div class="fl-page-nav-wrap">
        <nav class="fl-page-nav navbar navbar-default">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".fl-page-nav-collapse">
                <span>Menu</span>
            </button>
            <?php wp_nav_menu(array("menu"=>$attr["menu"],"menu_class"=>"nav navbar-nav menu","container_class"=>"fl-page-nav-collapse collapse navbar-collapse")); ?>
        </nav>
    </div>
</div>